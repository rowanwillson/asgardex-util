# `@thorchain/asgardex-util`

Utitity helpers for ASGARDEX clients

## Modules (in alphabetical order)

- `memo` - Utilities for memos
- `stake` - XYK formula calc for stake
- `swap` - XYK formula calc for swap

## Usage

**Calculations-Staking**

```
import {
  PoolData,
  getSwapOutput,
  getSwapOutputWithFee,
  getSwapInput,
  getSwapSlip,
  getSwapFee,
  getValueOfAssetInRune,
  getValueOfRuneInAsset,
  getDoubleSwapOutput,
  getDoubleSwapOutputWithFee,
  getDoubleSwapInput,
  getDoubleSwapSlip,
  getDoubleSwapFee,
  getValueOfAsset1InAsset2,
} from '@thorchain/asgardex-util'
```

**Calculations-Swapping**

```
import {
  UnitData,
  StakeData,
  getStakeUnits,
  getPoolShare,
  getSlipOnStake
} from '@thorchain/asgardex-util'
```

## Installation

```
yarn add @thorchain/asgardex-util
```
